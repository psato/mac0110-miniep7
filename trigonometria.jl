# MAC0110 - MiniEP7
# Pedro Rabello Sato - 10262610

function sin(x)
    result = 0
    n = big(0)
    nextsum = x
    while !(-0.000001 < nextsum <  0.000001)
        result += nextsum
        n += 1
        nextsum = ((-1)^n) * (x^(2n+1)/factorial(2n+1))
    end
    return result
end

function cos(x)
    result = 0
    n = big(0)
    nextsum = 1
    while !(-0.000001 < nextsum <  0.000001)
        result += nextsum
        n += 1
        nextsum = ((-1)^n) * (x^(2n)/factorial(2n))
    end
    return result
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
    result = 0
    n = big(1)
    nextsum = x
    while !(-0.0000001 < nextsum <  0.0000001)
        result += nextsum
        n += 1
        nextsum = ((2^(2*n)) * ((2^(2*n)) - 1 ) * bernoulli(n) * (x^(2n-1)))/(factorial(2*n))
    end
    return result
end

function check_sin(value, x)
    return isapprox(sqrt(1-Base.cos(x)^2), value, atol=1e-9)
end

function check_cos(value, x)
    return isapprox(sqrt(1-Base.sin(x)^2), value, atol=1e-9)
end

function check_tan(value, x)
    return isapprox(Base.sin(x)/Base.cos(x), value, atol=1e-9)
end

function sin_taylor(x)
    result = 0
    n = big(0)
    nextsum = x
    while !(-0.000001 < nextsum <  0.000001)
        result += nextsum
        n += 1
        nextsum = ((-1)^n) * (x^(2n+1)/factorial(2n+1))
    end
    return result
end

function cos_taylor(x)
    result = 0
    n = big(0)
    nextsum = 1
    while !(-0.000001 < nextsum <  0.000001)
        result += nextsum
        n += 1
        nextsum = ((-1)^n) * (x^(2n)/factorial(2n))
    end
    return result
end

function tan_taylor(x)
    result = 0
    n = big(1)
    nextsum = x
    while !(-0.0000001 < nextsum <  0.0000001)
        result += nextsum
        n += 1
        nextsum = ((2^(2*n)) * ((2^(2*n)) - 1 ) * bernoulli(n) * (x^(2n-1)))/(factorial(2*n))
    end
    return result
end

using Test
function test()

    x = π
    while x <= π
        @test isapprox(trigonometria.sin(x),Base.sin(x), atol=1e-5)
        x+=0.0001
    end
    @test isapprox(trigonometria.sin(π),Base.sin(π), atol=1e-5)
    println("Test passed for sin()")

    x = π
    while x <= π
        @test isapprox(trigonometria.cos(x),Base.cos(x), atol=1e-5)
        x+=0.0001
    end
    @test isapprox(trigonometria.cos(π),Base.cos(π), atol=1e-5)
    println("Test passed for cos()")

    x = -π/2 + 0.1
    while x < π/2 - 0.1
        @test isapprox(trigonometria.tan(x),Base.tan(x), atol=1e-5)
        x+=0.01
    end
    println("Test passed for tan()")
end
